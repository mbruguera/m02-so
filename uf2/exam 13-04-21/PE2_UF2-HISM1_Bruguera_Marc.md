<<<<<<< HEAD
## Autor:          PE2_UF2_Grup_Cognom_alumne.md
## Date:           13/04/21
## Description:    Prova M02 - UF02 Linux 1  
## Entrega Prova escrita:   

1. **( 1 punt ) Copia aquest fitxer en el teu repositori remot ( GitLab, GitHub ), a la carpeta /m-02/uf2/prova-escrita/**
**Escriu aquí el link.**
https://gitlab.com/mbruguera/m02-so/-/tree/master/exam%2013-04-21

2. **(1 punt ) Escriu un script que ens dongui informació sobre la caducitat i validesa dels usuaris del Sistema.**
**Si l'ordre retorna un estat de sortida 0, informeu que "l'ordre s'ha realitzat correctament" i sortiu amb un estat de sortida 0.**  
**Si l'ordre retorna un estat de sortida diferent de zero, informeu "Comanda fallida" i sortiu amb un estat de sortida 1.**

sudo cat /etc/shadow
if [ $? -eq 0 ]; then
	echo "Command done"
	exit 0
else
	echo "Command error"
	exit 1
fi


3. **( 1 punt ) Quin cron farem servir per tal que l'script anterior es faci a les 23:45 cada diumenge?** 

45 23 * * 7


4. **(1 punt ) Feu un script que comprovi si existeix el fitxer "/tmp/prova". Si no existeix el crea. Feu que informi a l'usuari del que ha passat.**

if [ -e /tmp/prova ]; then
	echo "Esta tot correcte"
else
	echo "No existeix ningun file dit prova a /tmp, el vaig a crear"
	$(touch /tmp/prova)
fi

5. **(1 punt ) Que estem fent en la següent comanda? grep -w 8 fitxer**
Força al grep a buscar la linea 8 del fitxer "fitxer".


6. **(1 punt ) Com comprovem que la anterior ordre s'hagi executat amb èxit?.**

COMMAND=$(grep -w 8 fitxer)
if [ $COMMAND -eq 1 ]; then
	echo "TOT CORRECTE"
	echo $COMMAND
else
	echo "ERROR"
	exit 1 
fi


7. **(1 punt ) Crea un fitxer .txt de nom la data i hores actuals a /tmp**
touch $(date +"%m%d%Y%T").txt /tmp


8. **(1 punt ) Fer un script que rep tres arguments i valida que siguin exactament 3.**
if [ $# -eq 3 ]; then
	echo "OK" >> $1
	echo "OK" >> $1
	echo "OK" >> $1
else 
	echo "too many argument, only 3"
fi

9. **(1 punt ) Que fa la següent comanda? cat file1.txt file2.txt file3.txt | sort > file4.txt**

Ordenar per ordre alfabetic lo que hi ha al file1.txt, file2.txt i file3.txt i volca la informació a file4.txt amb l'ordre.

10. **(1 punt ) Si tenim en un fitxer el següent contingut:**

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
gener   febrer  març    abril
```

**Quina ordre hem d'executar per obtenir**

```
two
beta
febrer
```

cut -f 2



=======
```
## Autor:          PE2_UF2_HISM1_Bruguera_Marc.md
## Date:           13/04/21
## Description:    Prova M02 - UF02 Linux 1  
## Entrega Prova escrita:   
```
1. **( 1 punt ) Copia aquest fitxer en el teu repositori remot ( GitLab, GitHub ), a la carpeta /m-02/uf2/prova-escrita/**
**Escriu aquí el link.**
https://gitlab.com/mbruguera/m02-so/-/tree/master/exam%2013-04-21

2. **(1 punt ) Escriu un script que ens dongui informació sobre la caducitat i validesa dels usuaris del Sistema.**
**Si l'ordre retorna un estat de sortida 0, informeu que "l'ordre s'ha realitzat correctament" i sortiu amb un estat de sortida 0.**  
**Si l'ordre retorna un estat de sortida diferent de zero, informeu "Comanda fallida" i sortiu amb un estat de sortida 1.**

sudo cat /etc/shadow
if [ $? -eq 0 ]; then
	echo "Command done"
	exit 0
else
	echo "Command error"
	exit 1
fi


3. **( 1 punt ) Quin cron farem servir per tal que l'script anterior es faci a les 23:45 cada diumenge?** 

45 23 * * 7


4. **(1 punt ) Feu un script que comprovi si existeix el fitxer "/tmp/prova". Si no existeix el crea. Feu que informi a l'usuari del que ha passat.**

if [ -e /tmp/prova ]; then
	echo "Esta tot correcte"
else
	echo "No existeix ningun file dit prova a /tmp, el vaig a crear"
	$(touch /tmp/prova)
fi

5. **(1 punt ) Que estem fent en la següent comanda? grep -w 8 fitxer**
Força al grep a buscar la linea 8 del fitxer "fitxer".


6. **(1 punt ) Com comprovem que la anterior ordre s'hagi executat amb èxit?.**

COMMAND=$(grep -w 8 fitxer)
if [ $COMMAND -eq 1 ]; then
	echo "TOT CORRECTE"
	echo $COMMAND
else
	echo "ERROR"
	exit 1 
fi


7. **(1 punt ) Crea un fitxer .txt de nom la data i hores actuals a /tmp**
touch $(date +"%m%d%Y%T").txt /tmp


8. **(1 punt ) Fer un script que rep tres arguments i valida que siguin exactament 3.**
if [ $# -eq 3 ]; then
	echo "OK" >> $1
	echo "OK" >> $1
	echo "OK" >> $1
else 
	echo "too many argument, only 3"
fi

9. **(1 punt ) Que fa la següent comanda? cat file1.txt file2.txt file3.txt | sort > file4.txt**

Ordenar per ordre alfabetic lo que hi ha al file1.txt, file2.txt i file3.txt i volca la informació a file4.txt amb l'ordre.

10. **(1 punt ) Si tenim en un fitxer el següent contingut:**

```
one	two	three	four	five
alpha	beta	gamma	delta	epsilon
gener   febrer  març    abril
```

**Quina ordre hem d'executar per obtenir**

```
two
beta
febrer
```

cut -f 2



>>>>>>> 4ebeb632fb3e5cbd7d9c93560c2c9eebe024fd08
