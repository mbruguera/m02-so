#/bin/bash/!
$NOW=$(date +"%Y%m%d")
echo "Quin prefix té l'arxiu?"
read -p "Prefix: " prefix
echo "Quina extensió de fitxer vols?"
read -p "Extensió: " extension
echo "El prefix que es busca es $prefix"
echo "L'extensió que es busca es $extension"
echo "A quin directori?"
read -p "Directori: " directorio
cd $directorio

for file in $directorio/*.$extension
do
        echo "Cambiem el nom  $file a ${NOW}-${file}
	if [ -z $prefix ]; then
		mv $file ${DAY}-${file}
	else 
		mv $file $prefix-${file}
		
	fi	
done
