#!/bin/bash
# VARAIBLES
CORRECTNUM='^[0-9]+$'
echo "==== MENU ======================================="
echo "Digues quina opció vols pel arxiu $1"
echo "1. Insereix números a un fitxer"
echo "2. Esborra númeors del fitxer"
echo "3. Cerca de numeros en el fitxer"
echo "4. Visualitza el contingut del fitxer"
echo "5. Sortir"
echo "================================================="
read -p "Selecciona una opció: " op
case $op in
	1)
		read -p "Digues quin numero vols inserir: " num
		if ! [[ $num =~ $CORRECTNUM ]] ; then
   			echo "error: Not a number" >&2; exit 1
		else
			echo $num >> $1
		fi
		;;
	2)
		echo "$(cat /dev/null > $1)"
		;;
	3)
		read -p "Quin numero vols trobar en el fitxer $1? " num
		echo "$(cat $1 | grep $num)"
		;;
	4)	
		echo "$(cat $1 | sort | uniq)"
		;;
	5)
		exit 0
		;;
esac

