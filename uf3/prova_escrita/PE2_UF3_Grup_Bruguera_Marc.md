## Autor:          PE2_UF3_Grup_Cognom_alumne.md
## Date:           28/05/21
## Description:    Prova M03 - UF03 SystemD, Journal, Firewall, Quotes
## Entrega Prova escrita UF3

En finalitzar l'exercici, el fitxer l'heu d'annexar al Moddle, **anomena'l amb el següent format: PE2_UF3_Grup_Cognom_nom.md**
També, recorda pujar-ho al GIT, a la carpeta UF3/prova-escrita/

## Enunciat

Estem encarregats d'administrar el sistema operatiu linux d'un servidor a la nostra empresa. Se'ns demana que fem diverses coses per tal d'obtenir un servidor amb les funcionalitats que es requereixen:

- Cal un servidor web.
- Caldrà establir un tallafocs per tal que no ens accedeixin a ports que no siguin públics.
- Caldrà un accés de ssh per a la gestió remota d'aquest servidor, però volen que només sigui accessible en horari d'oficina (tot i que la màquina estarà en funcionament tot el dia)

1. [2 punts] **Cal que aquesta màquina vostra tingui accés per ssh i que estigui en hora. Per això comprovarem els serveis:**

    - [0,25 punts] Ordre per activar el servei de ssh en iniciar el sistema operatiu: 
	systemctl active ssh
    - [0,25 punts] Ordre per arrencar ara el servei de ssh:
	systemctl start ssh
    - [0,25 punts] Odre i sortida per veure l'estat del servei ssh:
	systemctl status ssh
```
	a191019mb@a09:~$ sudo systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; disabled; vendor preset: enabled)
   Active: active (running) since Fri 2021-05-28 10:09:28 CEST; 1s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
  Process: 4712 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
 Main PID: 4713 (sshd)
    Tasks: 1 (limit: 4915)
   Memory: 2.0M
   CGroup: /system.slice/ssh.service
           └─4713 /usr/sbin/sshd -D

May 28 10:09:28 a09 systemd[1]: Starting OpenBSD Secure Shell server...
May 28 10:09:28 a09 sshd[4713]: Server listening on 0.0.0.0 port 22.
May 28 10:09:28 a09 sshd[4713]: Server listening on :: port 22.
May 28 10:09:28 a09 systemd[1]: Started OpenBSD Secure Shell server.
```
    - [0,25 punts] Ordre per activar el servei d'hora en iniciar el sistema operatiu:
	systemctl active ntpd
    - [0,25 punts] Ordre per arrencar ara el servei d'hora:
	systemctl start ntpd
    - [0,25 punts] Ordre i sortida per veure l'estat del servei d'hora:
	systemctl status ntpd
```
[isard@f32-isard ~]$ sudo systemctl status ntpd
● ntpd.service - Network Time Service
     Loaded: loaded (/usr/lib/systemd/system/ntpd.service; disabled; vendor pre>
     Active: active (running) since Fri 2021-05-28 10:17:11 CEST; 2s ago
    Process: 32145 ExecStart=/usr/sbin/ntpd -u ntp:ntp $OPTIONS (code=exited, s>
   Main PID: 32147 (ntpd)
      Tasks: 2 (limit: 4666)
     Memory: 1.9M
        CPU: 14ms
     CGroup: /system.slice/ntpd.service
             └─32147 /usr/sbin/ntpd -u ntp:ntp -g

May 28 10:17:11 f32-isard ntpd[32147]: Listen normally on 2 lo 127.0.0.1:123
May 28 10:17:11 f32-isard ntpd[32147]: Listen normally on 3 enp1s0 192.168.122.>
May 28 10:17:11 f32-isard ntpd[32147]: Listen normally on 4 lo [::1]:123
May 28 10:17:11 f32-isard ntpd[32147]: Listen normally on 5 enp1s0 [fe80::3b89:>
May 28 10:17:11 f32-isard ntpd[32147]: Listening on routing socket on fd #22 fo>
May 28 10:17:11 f32-isard ntpd[32147]: kernel reports TIME_ERROR: 0x41: Clock U>
May 28 10:17:11 f32-isard ntpd[32147]: kernel reports TIME_ERROR: 0x41: Clock U>
May 28 10:17:11 f32-isard systemd[1]: Started Network Time Service.
May 28 10:17:12 f32-isard ntpd[32147]: Soliciting pool server 185.132.136.116
May 28 10:17:13 f32-isard ntpd[32147]: Soliciting pool server 162.159.200.1
```
    - [0,5 punts] Ordre per veure tots els seveis que es troben arrencats actualment:
	systemctl list-units --type service --all

2. [2 punts] **Instal.leu ara el servidor web nginx. Aquest servidor ens permetrà tenir un servei en producció:**

    - [0,50 punts] Ordre per instal.lar el servidor web nginx al vostre sistema operatiu:
	sudo dnf install nginx
    - [0,50 punts] Ordre per activar el servei nginx en iniciar el sistema operatiu:
	systemctl active nginx
    - [0,50 punts] Ordre per arrencar ara el servei nginx:
	systemctl start nginx
    - [0,50 punts] Ordre i sortida per a veure els últims 10 missatges de log del servei nginx:
	

3. [2 punts] **Ara cal assegurar el servei ssh i el web com a els únics als que permetem accés:**

   - [0,25 punts] Ordre per activar el servei de tallafocs en iniciar el sistema operatiu:
	systemctl active firewalld
   - [0,25 punts] Ordre per arrencar ara el servei de tallafocs:
	systemctl start firewalld
   - [0,25 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa:
```
[isard@f32-isard ~]$ sudo firewall-cmd --list-ports --zone=public

[isard@f32-isard ~]$ 
```
   - [0,5 punts] Ordre(s) per tal d'aconseguir que només siguin accessibles els ports 22 i 80 del servidor a partir de la informació mostrada en l'ordre anterior:
	firewall-cmd --zone=public --add-port=22/tcp --permanent
	firewall-cmd --zone=public --add-port=80/tcp --permanent

   - [0,5 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa:
   - [0,25 punts] Feu que aquesta configuració sigui permanent. Indiqueu la/les ordre(s) per a fer-ho:
	firewall-cmd --runtime-to-permanent 
4. [2 punts] **Feu ara dos scripts que comprovin l'estat del servei nginx. Un l'haurà d'arrencar si no ho està i l'altre l'haurà d'aturar si està arrencat**
```
if (systemctl -q is-active nginx.service)
    then
    echo "Esta encés, l'apago"
    exit 1
fi
```
    - [0,5 punts] Script que comprova si està aturat i l'arrenca

    - [0,5 punts] Què hauríem de canviar al nostre script per tal que enviés un missatge de prioritat 'info' al journal amb informació que ens digui si l'hem arrencat o si l'hem parat?
```
if (systemctl -q is-active nginx.service)
    then
    echo "Esta encés, l'apago"
    exit 1
    $(journalctl -p 5)
fi
```
    - [0,5 punts] Com podem veure els missatges de prioritat 'info' del log?
sudo jounalctl -p 5
5. [2 punts]  **Una mica sobre quotes:** 
   - [0,25 punts] Per a que serveixen les quotes de disc?
	En informàtica, una quota de disc és un límit establert per l'administrador de sistema que 	restringeix certs aspectes de l'ús de sistema d'arxius en els sistemes operatius moderns.
   - [0,50 punts] creem un disc virtual de 80Mb anomenat examen a la carpeta /home/
	sudo dd if=/dev/zero of=/home/users/inf/hism1/a191019mb/lisa bs=80k count=100
   - [0,25 punts] Al disc virtual que acabem de crear li donem format ext4
	sudo mkfs.ext4 /home/users/inf/hism1/a191019mb/lisa
   - [0,50 punts] Montem l'arxiu examen a la carpeta /mnt/usuari
	mkdir /mnt/usuari
	sudo mount -o loop /home/users/inf/hism1/a191019mb/lisa /mnt/usuari
   - [0,25 punts] Quin fitxer hem de modificar, per tal de que es munti el disc automàticament en reiniciar el sistema. 
	/etc/fstab
   - [0,25 punts] Que és un inode?
	
Es un node de index o numero de index que es una estructura de dades en un sistema Linux que emmagatzema l'informació sobre un archiu o directori.



