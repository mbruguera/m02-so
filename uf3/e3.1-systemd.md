# E3. Exercici 1. SystemD

## Introducció

## Continguts

Amb els fiters i directoris de la carpeta exercici responeu les segûents preguntes:

Indiqueu en cada pregunta l'ordre i també el resultat obtingut (podeu copiar del terminal)

## Entrega

1. ** Instal.leu al vostre ordinador el servidor web apache2 (httpd). Assegureu-vos que el teniu enabled**.
   - Ordre per instal.lar-lo: 
	(FED) sudo dnf install httpd
	(DEB) sudo apt install apache2
   - Ordre per posar-lo a 'enabled': 
	systemctl enable apache2
   - Ordre i sortida per veure'n l'estat: 
	systemctl status apache2

![screenshot1](https://gitlab.com/mbruguera/m02-so/-/raw/master/img/Screenshot%20terminal%201.png)

2. ** Creeu ara un servei i que arrenqui un servidor web python amb el mòdul SimpleHTTPServer. Feu que el fitxer estigui a /usr/lib/systemd/system.**
  - Contingut del fitxer:
    [Unit]
      Description=El meu servidor web
    [Service]
      ExecStart=/usr/bin/python3 -m http.server
    [Install]
      WantedBy=multi-user.target

3. ** Feu que aquest servidor s'activi en iniciar-se el sistema. **
  - Ordre per fer que s'iniciï amb el sistema: 
	systemctl enable myweb.service
  - Expliqueu què ha fet aquesta ordre: 
	Que estigui actiu el servei al iniciar el PC

4. ** Reinicieu el vostre ordinador. Comproveu com es troben els dos serveis, httpd i myweb.**
  - Ordre i sortida de l'estat de httpd: 
	systemctl status httpd
  - Ordre i sortida de l'estat de myweb: 
	systemctl status myweb.Service
  - Expliqueu què ha passat. 
	Esta actiu al iniciar al PC

![Screenshot](https://gitlab.com/mbruguera/m02-so/-/raw/master/img/screenshot2.png)

5. ** Mostreu l'arbre d'arrencada de serveis on es vegi quin dels serveis s'ha iniciat abans.**
  - Ordre i sortida: 
	ps -eaf | grep [s]ystemd
	
	ps = reportar una instantánea de los procesos actuales
	-ef = Para ver todos los procesos del sistema utilizando la sintaxis estándar
	-a = Seleccione todos los procesos excepto los líderes de sesión (consulte getsid (2)) y los procesos no asociados con una terminal.
	
	o
		
	systemd-analyze critical-chain
![Screenshot](https://gitlab.com/mbruguera/m02-so/-/raw/master/img/Screenshot%20from%202021-04-27%2011-27-06.png)

6. ** Desactiveu el servei httpd de l'arrencada i reinicieu.**
  - Ordre i sortida de l'estat de httpd: 
	systemctl stop httpd
  - Ordre i sortida de l'estat de myweb: 
	systemctl stop myweb.service
  - Expliqueu què ha passat. 
	No inicia el servei al arrancar el PC

7. ** Feu que el vostre sistema s'iniciï amb el 'target' multi-user i reinicieu per veure què ha passat.**
  - Ordre per establir per defecte el multi-user: 
	systemctl set-default multi-user.target
  - Expliqueu què ha passat en reiniciar.
      S'inicia un terminal (en pantalla de terminal i no en pantalla gràfica)

8. ** Retorneu el vostre sistema al 'target' graphical i reinicieu.**
  - Ordre per establir per defecte el multi-user: 
	systemctl set-default graphical.target
  - Expliqueu què ha passat en reiniciar. 
	Torna a la 'interfaz' grafica per defecte
  
